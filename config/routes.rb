Rails.application.routes.draw do
  get 'animal/show'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  get '/spotinfy/:name', to: "static_pages#home", as: :spotinfy
end
